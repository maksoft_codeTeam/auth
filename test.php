<?php
include __DIR__.'/../../../../vendor/autoload.php';
date_default_timezone_set('Europe/Sofia');

use Maksoft\lib\auth\Login;
use Maksoft\lib\auth\Logging;
use Maksoft\lib\auth\FailedAttempts;
use Pimple\Container;
use Monolog\Logger;
use Monolog\Handler\FirePHPHandler;

$container = new Container();
$container->register(new Maksoft\Service\DiContainer());
$logger =  new Logger('auth');
$pdoHandler = new Maksoft\lib\PDOLogger($container['database']);
$login = new Login($container['Gateway']);
$login->attach( new Logging($logger, $pdoHandler) );
$login->attach( new FailedAttempts($container['Gateway']));
 
if ( $login->init( "ryordano", "admin0", "127.0.0.1" ) ) {
    echo "User logged in!";
} else {
    echo "<pre>";
    print_r( $login->getStatus() );
    echo "</pre>";
}

