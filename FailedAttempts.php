<?php
namespace Maksoft\lib\auth;
use Maksoft\Gateway\Gateway;

class FailedAttempts implements \SplObserver
{
    const FAILED_ATTEMPTS = 5;
    const BLOCK_TIME = 60;

    protected $gate;

    public function __construct(Gateway $auth)
    {
        $this->gate = $auth;
    } 

    public function update( \SplSubject $SplSubject )
    {
        $status = $SplSubject->getStatus();
        $event = get_class($SplSubject);

        switch ( $status[0] ) {
            case Login::INCORRECT_PWD:
                $this->proccessFlood($status, $event);
                break;
 
            case Login::UNKNOWN_USER:
                $this->proccessFlood($status, $event);
                break;
 
            case Login::ALREADY_LOGGED_IN:
                break;
            case Login::ALLOW:
                $this->gate->auth()->removeBlock($status[2], $event);
                $this->gate->auth()->removeFailedAttempts($status[2], $event);
                break;
        }
        return;
    }

    protected function proccessFlood($status, $event)
    {
        $ip = $status[2];
        $is_blocked = $this->isBlocked($ip, $event);
        if($is_blocked){
            throw new \Maksoft\errors\MaksoftCmsLoginException(
                sprintf("Your ip address (%s) is blocked until %s", 
                $ip, date('m/d/Y H:i:s', $is_blocked->block_until))); 
        }
        if($this->gate->auth()->countFailedAttempts($ip, $event) >= 5){
            $this->gate->auth()->blockIp($ip, time()+self::BLOCK_TIME, $event);
            throw new \Maksoft\errors\MaksoftCmsLoginException(
                sprintf("Your ip address (%s) is blocked for %d seconds", $ip, self::BLOCK_TIME));
        }
        $this->gate->auth()->registerFailedAttempt($status[1], $ip, $event);
        throw new \Maksoft\errors\MaksoftCmsLoginException(
            sprintf("Your ip address (%s) have only %d attempts more. So be carefull!", $ip,
                self::FAILED_ATTEMPTS-$this->gate->auth()->countFailedAttempts($ip, $event)));
    }

    protected function isBlocked($ip, $event)
    {
        $block = $this->gate->auth()->isBlocked($ip, $event);
        if(!$block){
            $this->gate->auth()->removeFailedAttempts($ip, $event);
            $this->gate->auth()->removeBlock($ip, $event);
            return false;
        }
        return $block;
    }
}

