<?php
namespace Maksoft\lib\auth;
use Webmozart\Assert\Assert;
use Maksoft\Gateway\Gateway;


class Login implements \SplSubject 
{
    const UNKNOWN_USER = 1;
    const INCORRECT_PWD = 2;
    const ALREADY_LOGGED_IN = 3;
    const ALLOW = 4;
    const NOT_LOGGED = 5;
 
    private $status = array();
    private $storage;
    protected $auth;
    private $user_data;
 
    public function __construct(Gateway $auth)
    {
        $this->gate = $auth;
        $this->storage = new \SplObjectStorage();
    }
 
    public function init($username, $password, $ip)
    {
        /*
         * First validate username and password
         * */
        $status = 4;
        try{
            $this->isLogged();
            $this->usernameValidator($username);
            $this->passwordValidator($password);
            $user = $this->gate
                ->auth()
                ->getLogin($username, $password, $this->gate->site()->getData()->SitesID);
            $msg = 'successfully logged';
        } catch (\InvalidArgumentException  $e){
           $status = $e->getCode();
           $msg = $e->getMessage();
        } catch (\Maksoft\errors\MaksoftCmsException $e ){
            $status = $e->getCode();
            $msg = $e->getMessage();
        } catch (\Maksoft\errors\MaksoftCmsLoginException $e){
            $status = $e->getCode();
            $msg = $e->getMessage();
        }      
        $this->setStatus($status, $username, $ip, $msg, $user);
        // Notify all the observers of a change
        try{
            $this->notify();
        } catch (\Maksoft\errors\MaksoftCmsLoginException $e){
            $this->setStatus(6, $username, $ip, $e->getMessage(), $this->user_data);
        }
        if ($this->status[0] == self::ALLOW){
            return true;
        }
        return false;
    }

    protected function usernameValidator($username)
    {
        try{
            Assert::stringNotEmpty($username, 'Username cant be null');
            Assert::minLength($username, 3, 'Username must be atleast 3 characters');
            Assert::maxLength($username, 20, 'Username must be maximum 20 characters!');
        } catch (\InvalidArgumentException $e){
            throw new \InvalidArgumentException($e->getMessage(), 1);
        }
        return $username;
    }

    protected function passwordValidator($password)
    {
        try{
            Assert::stringNotEmpty($password, 'Password cant be null');
            Assert::minLength($password, 3, 'Password must be atleast 3 characters');
            Assert::maxLength($password, 15, 'Password must be maximum 20 characters!');
         } catch (\InvalidArgumentException $e){
            throw new \InvalidArgumentException($e->getMessage(), 2);
         }
        return $password;       
    }

    public function isLogged()
    {
        $response = $this->gate->auth()->isLogged();
        if($resonse)
            throw new \Maksoft\errors\MaksoftCmsLoginException("Already logged", 3);
        return false;
    }

    private function setStatus( $status, $username, $ip, $msg, $user)
    {
        $this->status = array( $status, $username, $ip , $msg, $user);
    }
 
    public function getStatus()
    {
        return $this->status;
    }
 
    public function attach( \SplObserver $observer )
    {
        $this->storage->attach( $observer );
    }
 
    public function detach( \SplObserver $observer )
    {
        $this->storage->detach( $observer );
    }
 
    public function notify()
    {
        foreach ( $this->storage as $observer ) {
            $observer->update( $this );
        }
    }

}
