<?php
namespace Maksoft\lib\auth;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\FirePHPHandler;


class Logging implements \SplObserver
{
    public function __construct(Logger $logger, \Maksoft\lib\PDOLogger $pdoHandler)
    {
        $this->logger = $logger;
        $this->logger->pushHandler($pdoHandler);
        $this->logger->pushHandler(new FirePHPHandler());
    }

    public function update( \SplSubject $SplSubject )
    {
        $status = $SplSubject->getStatus();
        $this->event = get_class($SplSubject);
        $this->logger->addInfo($status[3]);
    }
}
