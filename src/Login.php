<?php
namespace Auth;
use Webmozart\Assert\Assert;


class Login implements \SplSubject 
{
    const UNKNOWN_USER = 1;
    const INCORRECT_PWD = 2;
    const ALREADY_LOGGED_IN = 3;
    const ALLOW = 4;
 
    private $status = array();
    private $storage;
 
    public function __construct()
    {
        $this->storage = new \SplObjectStorage();
    }
 
    public function init($username, $password, $ip)
    {
        /*
         * First validate username and password
         * */
        $status = 4;
        try{
           $this->usernameValidator($username);
           $this->passwordValidator($password);
        } catch (\InvalidArgumentException  $e){
           $status = $e->getCode();
           $msg = $e->getMessage();
        }
        // Let's simulate different login procedures
        $this->setStatus( RAND(1,4), $username, $ip, $msg);
        // Notify all the observers of a change
        $this->notify();
        if ( $this->status[0] == self::ALLOW ){
            return true;
        }
        return false;
    }

    protected function usernameValidator($username)
    {
        try{
            Assert::stringNotEmpty($username, 'Username cant be null');
            Assert::minLength($username, 3, 'Username must be atleast 3 characters');
            Assert::maxLength($username, 20, 'Username must be maximum 20 characters!');
            #Assert::allnum($username, 'Invalid username! Username must contain letters and digits only!');
        } catch (\InvalidArgumentException $e){
            throw new \InvalidArgumentException($e->getMessage(), 1);
        }
        return $username;
    }

    protected function passwordValidator($password)
    {
        try{
            Assert::stringNotEmpty($password, 'Password cant be null');
            Assert::minLength($password, 3, 'Password must be atleast 3 characters');
            Assert::maxLength($password, 15, 'Password must be maximum 20 characters!');
         } catch (\InvalidArgumentException $e){
            echo $e->getMessage();
            throw new \InvalidArgumentException($e->getMessage(), 2);
         }
        return $password;       
    }

    public function isRegistered()
    {
    }

    private function setStatus( $status, $username, $ip, $msg )
    {
        $this->status = array( $status, $username, $ip , $msg);
    }
 
    public function getStatus()
    {
        return $this->status;
    }
 
    public function attach( \SplObserver $observer )
    {
        $this->storage->attach( $observer );
    }
 
    public function detach( \SplObserver $observer )
    {
        $this->storage->detach( $observer );
    }
 
    public function notify()
    {
        foreach ( $this->storage as $observer ) {
            $observer->update( $this );
        }
    }
}
